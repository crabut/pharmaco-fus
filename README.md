%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                   %
%                                                                                   %
%       SVM classifier to distinguish pathological states from baseline state       % 
%                                                                                   %
%                                                                                   %
%                                                                                   %
%    Step 1: training the classifier with a training cohort                         %
%                                                                                   %
%    Input : +   connectivty matrices of the training cohort                        %
%                2D matrix: 10x10                                                   %
%                Baseline matrices assigned with score 0                            %
%                Scopolamine matrices assigned with score 1                         %
%    Output : + SVM Model                                                           %
%             + SVM coefficient matrix                                              %
%                                                                                   %
%                                                                                   %
%                                                                                   %
%                                                                                   %
%    Step 2: test the model with a validation cohort                                %      
%    Input : +   connectivty matrices of the validation cohort                      %
%                2D matrix: 10x10                                                   %
%                                                                                   %
%    Output : + score of the matrices                                               %
%                                                                                   %
%                                                                                   %
%                                                                                   %
%                                                                                   %
% Rabut et al.                                                       Pharmaco-fUS   %
%                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                 STEP 1                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 
% Longitudinal Study as training Matrices %% 

load('CBV_longitudinal_study.mat')

% Training Matrices
clear training

for i=1:5;
    training(i,:)=eval(['baseline_mouse_', num2str(i) ,'(:)'])';
end
t=1;
for i=6:10
    training(i,:)=eval(['scopo3_mouse_', num2str(t) ,'(:)'])';
    t=t+1;
end


% Score
clear Score
for i=1:5
    Score(i)=0;
end
for i=6:10
    Score(i)=1;
end



% SVM Model
clear Mdl
Mdl =  fitrsvm(training,Score)




% SVM Coefficient Matrix

clear coeff
for i = 1:size( Mdl.SupportVectors,2)
   coeff(i) = dot(Mdl.Alpha, Mdl.SupportVectors(:,i));
end

svm_coeff=reshape(coeff,[10 10]);
bias = Mdl.Bias;

figure
imagesc(svm_coeff)
colormap jet
caxis([-0.1 0.1])



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                 STEP 2                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Evaluation of the score on a second cohort

clear ToEvaluate
% 
for i=1:8
    ToEvaluate(i,:)=eval(['baseline_mouse', num2str(i) ,'(:)'])';
end

t=1;
for i=9:16
    ToEvaluate(i,:)=eval(['scopo_mouse', num2str(t) ,'(:)'])';
    t=t+1;
end

Evaluation=predict(Mdl,ToEvaluate);

figure; 
plot(Evaluation,'*')

